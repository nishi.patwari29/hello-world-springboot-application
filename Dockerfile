FROM openjdk:15

WORKDIR /app

COPY . .

RUN ./gradlew build

EXPOSE 8080

ENTRYPOINT ["java","-jar","build/libs/hello-world-0.1.0.jar"]
